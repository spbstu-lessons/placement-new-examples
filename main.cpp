#include <iostream>
#include "vector.hpp"


void printInfo(const int *ptr) {
  std::cout << "value: " << *ptr << " location: " << ptr << "\n";
}

class A {
public:
  A(int a) : a_(a) {}

  int a_;
};

int main() {
  unsigned char buf[sizeof(int) * 2];
  int *a = new(buf) int(5);
  int *b = new(buf + sizeof(int)) int(10);

  std::cout << "Info a\n";
  printInfo(a);

  std::cout << "Info b\n";
  printInfo(b);

  unsigned char *buffer1 = new unsigned char[sizeof(A) * 2];
  A *aA = reinterpret_cast<A *>(buffer1);

  new(aA) A(55);
  new(aA + 1) A(68);

  std::cout << "aA[0] " << aA[0].a_ << "\n";
  std::cout << "aA[1] " << aA[1].a_ << "\n";

  for (int i = 0; i < 2; i++) {
    aA[i].~A();
  }
  delete aA;

  Vector<A> vector{};
  vector.push_back(A(55));
  vector.push_back(A(-15));
  vector.push_back(A(-95));


  for (int j = 0; j < vector.size(); ++j) {
    std::cout << "vector[" << j << "].a_ = " << vector[j].a_ << "\n";
  }
}
